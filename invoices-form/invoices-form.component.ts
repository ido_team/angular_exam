import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Invoice } from '../invoice/invoice';
import { NgForm } from '@angular/forms';
import {InvoiceService} from '../invoice/invoice.service';

@Component({
  selector: 'app-invoices-form',
  templateUrl: './invoices-form.component.html',
  styleUrls: ['./invoices-form.component.css'],
  inputs:['invoice']
})
export class InvoicesFormComponent implements OnInit {



  constructor(private _invoiceService: InvoiceService) { }
i:number = 0
invoice: Invoice = {invoiceId:0 ,pname:'', amount: 0};
onSubmit(form:NgForm){

this.invoice = {invoiceId:this.i+1, pname:form.form.value.name, amount:form.form.value.amount};
  
 // console.log(form.form.value);
 //console.log(this.invoice)
 this._invoiceService.addInvoice(this.invoice);

 this.invoice = {invoiceId:0 ,pname:'', amount: 0};
}




  ngOnInit() {
  }

}
