import { Component, OnInit } from '@angular/core';
import {InvoiceService} from '../invoice/invoice.service';
import { Invoice } from '../invoice/invoice';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {


isLoading:boolean = true;
currentInvoice;

invoices: Invoice;
  constructor(private _invoiceService: InvoiceService) { }


  ngOnInit() {

    this._invoiceService.getInvoices().delay(0).subscribe(invoicesData =>

    {
      this.invoices = invoicesData;
      console.log(invoicesData);
      //this.isLoading = false;
      this.isLoading = false;
    }); 


    this.currentInvoice = -1;

  }

  addInvoice(invoice){
 console.log();
  this._invoiceService.addInvoice(invoice);
}
selectInvoice(invoice){
  if(this.currentInvoice == -1){
  this.currentInvoice = invoice;
     //console.log(user.id);
 
  }else{
    this.currentInvoice = -1;
  }
}

}
