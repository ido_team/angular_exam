import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';




@Injectable()
export class InvoiceService {

invoiceObservable;

getInvoices(){
  this.invoiceObservable = this.af.database.list('/invoices'); 
  return this.invoiceObservable;
}


addInvoice(invoice){
console.log(invoice);
  this.invoiceObservable.push(invoice);
}

  constructor(private af:AngularFire) { }

}
