import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule , Routes } from '@angular/router';
import { AngularFireModule } from 'angularfire2';


import { AppComponent } from './app.component';
//import { UsersComponent } from './users/users.component';
//import { DemoComponent } from './demo/demo.component';

//import { UsersService } from './users/users.service';
//import { UserComponent } from './user/user.component';
//import { PostsComponent } from './posts/posts.component';

//import { PostsService } from './posts/posts.service';
//import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
//import { UserFormComponent } from './user-form/user-form.component';
//import { PostFormComponent } from './post-form/post-form.component';
//import { ProductsComponent } from './products/products.component';
//import { ProductsService } from './products/products.service';
//import { ProductComponent } from './product/product.component';
//import { ProductFormComponent } from './product-form/product-form.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesFormComponent } from './invoices-form/invoices-form.component';
import { InvoiceService } from './invoice/invoice.service';

const appRoutes:Routes = [
  {path:'', component:InvoicesFormComponent},
  {path:'invoicesForm', component:InvoicesFormComponent},
  {path:'invoice', component:InvoiceComponent},
  {path:'**', component:PageNotFoundComponent}
];
export const firebaseconfig = {
    apiKey: "AIzaSyCT3Oxg8gu5gCNLIR06wZnu696_-5GiEpQ",
    authDomain: "exam-19e6e.firebaseapp.com",
    databaseURL: "https://exam-19e6e.firebaseio.com",
    storageBucket: "exam-19e6e.appspot.com",
    messagingSenderId: "858646874409"
  };

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    InvoiceComponent,
    InvoicesFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseconfig)
  ],
  providers: [InvoiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
